﻿using System;
using UnityEngine;

public class CamMov : MonoBehaviour
{
    public float speed = 2.0f;
    public float mouseSensitivity = 5.0f;

    private static int n = DiaSqu.n;
    private static int size = (int)System.Math.Pow(2, n);
    private static int halfSize = size / 2;

    // rotation around the Y axis
    private float rotY = 0.0f;
    // rotation around the X axis
    private float rotX = 0.0f;

    private Vector3 oldPosition;

    void Start()
    {
        // starting position of camera along the diagonal line of the map
        transform.position = new Vector3(halfSize, halfSize, n * 3);
        // setting rendering variables to new camera location
        GetComponent<Camera>().ResetWorldToCameraMatrix();
    }

    void Update()
    {
        oldPosition = transform.position;

        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = -Input.GetAxis("Mouse Y");

        rotY += mouseX * mouseSensitivity * Time.deltaTime;
        rotX += mouseY * mouseSensitivity * Time.deltaTime;


        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back * speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.Q))
        {
            transform.Rotate(Vector3.forward * Time.deltaTime * 90);
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.Rotate(Vector3.forward * Time.deltaTime * -90);
        }

        transform.Rotate(rotX * Vector3.right);
        transform.Rotate(rotY * Vector3.up);

        // if going out of bounds of landscape
        if (transform.position.x > size || transform.position.x < 0 || transform.position.y > size || transform.position.y < 0)
        {
            transform.position = oldPosition;
        }

    }
}