﻿using UnityEngine;
using System.Collections;

public class SunMov : MonoBehaviour
{
    public float speed = 400;
    private static int n = DiaSqu.n;
    private static int halfSize = (int)System.Math.Pow(2, n) / 2;
    private Vector3 halfBoard = new Vector3(halfSize, halfSize, 0);
    private Vector3 v;

    void Start()
    {
        // radius
        v = new Vector3(halfSize * 2, 0, 0);
    }
    
    void Update()
    {
        // sun rotates around the y axis
        v = Quaternion.AngleAxis(speed, Vector3.up) * v;
        transform.position = halfBoard + v;
    }
}
