﻿using UnityEngine;
using System.Collections;

public class PointLight : MonoBehaviour
{
    public Color color;
    private static int n = DiaSqu.n;
    private static int halfSize = (int)System.Math.Pow(2, n) / 2;

    void Start()
    {
        // starting position of sun should be aligned to the centre of the right side of the map
        transform.position = new Vector3(-halfSize, halfSize, 0);
    }
    public Vector3 GetWorldPosition()
    {
        return this.transform.position;
    }
}
