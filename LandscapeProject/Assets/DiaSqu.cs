﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DiaSqu : MonoBehaviour
{
    public PointLight sun;
    public Shader shader;
    public static int n = 3;
    private static int size = (int)System.Math.Pow(2, n) + 1;

    float[,] dataArray = new float[size, size];

    System.Random randGen = new System.Random();

    void Start()
    {
        // set the 4 corners
        dataArray[0, 0] = (((float)randGen.NextDouble() * 2) - 1) * n;
        dataArray[size - 1, 0] = (((float)randGen.NextDouble() * 2) - 1) * n;
        dataArray[0, size - 1] = (((float)randGen.NextDouble() * 2) - 1) * n;
        dataArray[size - 1, size - 1] = (((float)randGen.NextDouble() * 2) - 1) * n;
        DiamondSquareDataArray();

        Mesh newMesh = this.CreateFractalMesh();

        MeshFilter fractalMesh = this.gameObject.AddComponent<MeshFilter>();
        fractalMesh.mesh = newMesh;

        MeshRenderer renderer = this.gameObject.AddComponent<MeshRenderer>();
        renderer.material.shader = shader;

        MeshCollider collider = this.gameObject.GetComponent<MeshCollider>();
        collider.sharedMesh = newMesh;

    }


    void Update()
    {
        MeshRenderer renderer = this.gameObject.GetComponent<MeshRenderer>();

        // Pass updated light positions to shader
        renderer.material.SetColor("_PointLightColor", this.sun.color);
        renderer.material.SetVector("_PointLightPosition", this.sun.GetWorldPosition());
    }

    void DiamondSquareDataArray()
    {

        float value = 0;

        // value for reducing random noise after each square step
        float h = 1.0f;

        int sideLength;
        int x;
        int y;
        int halfSide;

        // creating heightmap
        for (sideLength = size - 1; sideLength >= 2; sideLength /= 2)
        {

            halfSide = sideLength / 2;

            // diamond values
            for (x = halfSide; x < size; x += sideLength)
            {
                for (y = halfSide; y < size; y += sideLength)
                {
                    value = dataArray[x - halfSide, y - halfSide];
                    value += dataArray[x - halfSide, y + halfSide];
                    value += dataArray[x + halfSide, y - halfSide];
                    value += dataArray[x + halfSide, y + halfSide];

                    value /= 4.0f;

                    // add random
                    value += (((float)randGen.NextDouble() * 2) - 1) / h;

                    dataArray[x, y] = value;
                }
            }

            // square values
            for (x = 0; x < size; x += halfSide)
            {
                // even columns
                if (x % sideLength == 0)
                {
                    for (y = halfSide; y < size; y += sideLength)
                    {
                        if (x != 0 && y != 0 && x != size - 1)
                        {
                            value = dataArray[x, y - halfSide];
                            value += dataArray[x - halfSide, y];
                            value += dataArray[x + halfSide, y];
                            value += dataArray[x, y + halfSide];

                            value /= 4.0f;

                        }
                        else if (x == 0)
                        {
                            value = dataArray[x, y - halfSide];
                            value += dataArray[x + halfSide, y];
                            value += dataArray[x, y + halfSide];

                            value /= 3.0f;

                        }
                        else if (x == size - 1)
                        {
                            value = dataArray[x, y - halfSide];
                            value += dataArray[x - halfSide, y];
                            value += dataArray[x, y + halfSide];

                            value /= 3.0f;
                        }

                        // add random
                        value += (((float)randGen.NextDouble() * 2) - 1) / h;

                        dataArray[x, y] = value;
                    }
                }
                // odd columns
                else
                {
                    for (y = 0; y < size; y += sideLength)
                    {
                        if (x != 0 && y != 0 && y != size - 1)
                        {
                            value = dataArray[x, y - halfSide];
                            value += dataArray[x - halfSide, y];
                            value += dataArray[x + halfSide, y];
                            value += dataArray[x, y + halfSide];

                            value /= 4.0f;

                        }
                        else if (y == 0)
                        {
                            value = dataArray[x - halfSide, y];
                            value += dataArray[x + halfSide, y];
                            value += dataArray[x, y + halfSide];

                            value /= 3.0f;

                        }
                        else if (y == size - 1)
                        {
                            value = dataArray[x, y - halfSide];
                            value += dataArray[x - halfSide, y];
                            value += dataArray[x + halfSide, y];

                            value /= 3.0f;
                        }

                        // add random
                        value += (((float)randGen.NextDouble() * 2) - 1) / h;

                        dataArray[x, y] = value;
                    }
                }

            }
            h *= 2.0f;
        }
    }

    Mesh CreateFractalMesh()
    {
        Mesh m = new Mesh();
        m.name = "Fractal";
        Vector3[] newVertices = new Vector3[3 * 2 * (size - 1) * (size - 1)];
        Vector3[] newNormals = new Vector3[newVertices.Length];
        Color[] newColors = new Color[newVertices.Length];
        float highPoint = 0;
        int k = 0;

        // for each row
        for (int j = 0; j < size - 1; j++)
        {
            // for each "square" on map
            for (int i = 0; i < size - 1; i++)
            {
                newVertices[k] = new Vector3((float)i, (float) j, dataArray[i, j]);
                if (dataArray[i, j] > highPoint)
                {
                    highPoint = dataArray[i, j];
                }
                newNormals[k] = newVertices[k].normalized;
                k++;

                newVertices[k] = new Vector3((float)i, (float)j + 1, dataArray[i, j + 1]);
                if (dataArray[i, j] > highPoint)
                {
                    highPoint = dataArray[i, j];
                }
                newNormals[k] = newVertices[k].normalized;
                k++;

                newVertices[k] = new Vector3((float)i + 1, (float)j + 1, dataArray[i + 1, j + 1]);
                if (dataArray[i, j] > highPoint)
                {
                    highPoint = dataArray[i, j];
                }
                newNormals[k] = newVertices[k].normalized;
                k++;

                newVertices[k] = new Vector3((float)i, (float)j, dataArray[i, j]);
                if (dataArray[i, j] > highPoint)
                {
                    highPoint = dataArray[i, j];
                }
                newNormals[k] = newVertices[k].normalized;
                k++;

                newVertices[k] = new Vector3((float)i + 1, (float)j + 1, dataArray[i + 1, j + 1]);
                if (dataArray[i, j] > highPoint)
                {
                    highPoint = dataArray[i, j];
                }
                newNormals[k] = newVertices[k].normalized;
                k++;

                newVertices[k] = new Vector3((float)i + 1, (float)j, dataArray[i + 1, j]);
                if (dataArray[i, j] > highPoint)
                {
                    highPoint = dataArray[i, j];
                }
                newNormals[k] = newVertices[k].normalized;
                k++;
            }

        }

        // assigning colours based on height
        for (int i = 0; i < newVertices.Length; i++)
        {
            if (newVertices[i].z < highPoint * 0.05)
            {
                newVertices[i].z = highPoint * 0.05f;
                newColors[i] = new Color(0, 0, 1, 0.5f);
            }
            else if (newVertices[i].z < highPoint * 0.1)
            {
                newColors[i] = Color.yellow;
            }
            else if (newVertices[i].z > highPoint * 0.9)
            {
                newColors[i] = Color.white;
            }
            else if (newVertices[i].z > highPoint * 0.6)
            {
                newColors[i] = Color.gray;
            }
            else
            {
                newColors[i] = Color.green;
            }
        }

        m.vertices = newVertices;
        m.normals = newNormals;
        m.colors = newColors;

        int[] triangles = new int[m.vertices.Length];
        for (int i = 0; i < m.vertices.Length; i++)
            triangles[i] = i;

        m.triangles = triangles;

        return m;
    }

    // fps = 60
    void Awake()
    {
        Application.targetFrameRate = 60;
    }
}
