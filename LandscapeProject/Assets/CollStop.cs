﻿using UnityEngine;
using System.Collections;

public class CollStop : MonoBehaviour {

    void Update()
    {
        transform.position = transform.parent.position;
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Land")
        {
            transform.parent.position = transform.parent.position + new Vector3(0, 0, 1);
        }
    }
}
