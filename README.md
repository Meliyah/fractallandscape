Graphics and Interaction Project 1
William Cao 696120

**Modelling of fractal landscape**

- created an empty object and attached a script, Dia Squ, to it

- Dia Squ responsible for implementing the diamond square algorithm

- first generated an array of height values, then made a mesh out of it

- starting corners were all set to a random number dependent on n (i.e. n in 2^n + 1)

- random noise added to each step is repeatedly scaled by half after every square step

**Camera motion**

- has been implemented according to the specifications

- E rolls the camera clockwise; Q rolls the camera anticlockwise

- please move your cursor to the centre of the screen before scene loads for optimal movement

**Surface properties**

- colour of terrain is based on the highest point:

          > 0.9 white (snow)

          0.9 > 0.6 gray (rock)

          0.6 > 0.1 green (grass)

          0.1 > 0.05 yellow (dirt/beach)

          < 0.05 blue (water)

- lighting has been implemented, using a modified version of the shader used in Lab 5

- sun is a sphere; moves around the centre of the terrain with an orbit radius of 2 * n

- unable to implement water transparency by time of submission (sorry)

- set to 60fps

** IN REGARDS TO GITIGNORE **
Not sure if I have implemented it properly. Followed instructions from this link http://stackoverflow.com/questions/21573405/how-to-prepare-a-unity-project-for-git
I have attached a zip folder containing my project as well.